# universis-reports

Universis API Server service for managing jasper server report templates and processing data reports.

## Usage

    npm i @universis/reports
    

## Configuration

Configure Universis API Server to use PrintService by adding a new entry under app.production.json#services section:

    "services": [
        ...
        {
            "serviceType": "@universis/reports#PrintService"
        }
        ...
    ]


Add `@universis/reports` settings under `app.production.json#settings.universis` section:

    "universis": {
        ...
        "reports": {
            "server": "https://example.com/jasperserver/",
            "user": "jasperadmin",
            "password": "jasperadmin",
            "rootDirectory": "reports/universis"
        }
        ...
    }
    
- reports.server: Set jasper server uri
- reports.user: Set jasper server user name
- reports.password: Set jasper server user password
- reports.rootDirectory: Set the root directory of jasper server reports that are going to be processed by PrintService
            